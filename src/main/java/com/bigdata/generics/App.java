package com.bigdata.generics;

import java.util.*;
public class App 
{
    public static void main( String[] args )
    {
        List iList = new ArrayList();
        iList.add(Integer.valueOf(1));
        iList.add(Integer.valueOf(2));
        Iterator iiter = iList.iterator();
        Integer i = iiter.next();
        Integer j = iiter.next();
        System.out.println(i+j);

        
        List sList = new ArrayList();
        sList.add("1");
        sList.add("2");
        Iterator siter = sList.iterator();
        String k = siter.next();
        String l = siter.next();
        System.out.println(k+l);
    }
}
