# Project initialization

The project was initialzed with
```
mvn archetype:generate -DgroupId=com.bigdata.generics -DartifactId=generics -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
```

# Build the project

The project is built with
```
mvn package
```
 The jar file is located in the folder target. It contains the project name and the version as stated in `pom-xml`.

 # Run the project

 If a java project is built, the compiler creates a platform independent bytecode. The inpendence is a great advantage since it can be executed on many machines, still it needs a program that can interpret and execute the bytecode. This is the Java Virtual Machine (JVM).

This program is named `java` on Linux, to start the program the jar-file `target/generics-1.0-SNAPSHOT.jar` and the application to be started `com.bigdata.generics.App` has to be to given:

 ```
 java -cp target/generics-1.0-SNAPSHOT.jar com.bigdata.generics.App
 ```